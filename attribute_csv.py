""" Script to generate csv file where the name is a specified feature class or shapefile. The columns in the output csv will be:
    1) column_name
    2) column_type
    3) description
    4) units
    5) coded_values
"""

import argparse
import attribute_fields_utils
import os


def main(inargs):
    """ Run the program with inargs passed to process function. """
    attribute_fields_utils.run(inargs.in_path, inargs.out_path, inargs.process, inargs.name)


if __name__ == "__main__":
    description = "Generate csv(s) with column headers that contains attribute information extracted from embedded metadata in spatial layer."
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("in_path", type=str,
                        help="Absolute path to directory or file geodatabase in which to process spatial data layers.")
    parser.add_argument("out_path", type=str,
                        help="Absolute path to directory in which to save output csv file(s).")
    parser.add_argument("process", type=str,
                        choices=["all", "one"],
                        help="Process ALL feature classes in geodatabase/shapefiles in directory or ONE feature class/shapefile.")
    parser.add_argument("-n", "--name", type=str, default=None,
                        help="Feature class or shapefile name to process.")

    args = parser.parse_args()
    if args.process == "one":
        assert (args.name is not None), f"-n argument must be provided to process a single layer."
    assert (os.path.exists(args.in_path)), f"{args.in_path} does not exist on filesystem."
    assert (os.path.exists(args.out_path)), f"{args.out_path} does not exist on filesystem."
    main(args)
