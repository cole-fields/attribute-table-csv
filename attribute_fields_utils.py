from encodings import utf_8
import arcpy
import csv
import sys
import os

CSV_HEADERS = ["column_name", "column_type", "description", "units", "coded_values"]

class SpatialLayers():
    def __init__(self, tables, layers, target_layer):
        self.tables = tables
        self.layers = layers
        self.target_layer = target_layer
        self.target_layer_name = self.get_lyr_name()
        self.fields = self.get_fields(target_layer)
        self.names = [self.get_field_name(f) for f in self.fields]
        self.types = [self.get_field_type(f) for f in self.fields]

    def target_exists(self):
        return self.target_layer in self.tables + self.layers

    def get_lyr_name(self):
        return self.target_layer.split(".")[0]

    def get_fields(self, lyr):
        try:
            return arcpy.ListFields(lyr)
        except OSError as e:
            sys.exit(e)

    def get_field_type(self, field):
            return field.type

    def get_field_name(self, field):
        return field.name
    
def create_csv(output_directory, sl_object):
    out_name = sl_object.target_layer_name + ".csv"
    out_file = os.path.join(output_directory, out_name)
    with open(out_file, newline="", encoding="utf_8", mode="w") as f:
        writer = csv.writer(f)
        writer.writerow(CSV_HEADERS)
        for i in range(len(sl_object.fields)):
            row = [sl_object.names[i], sl_object.types[i], None, None, None]
            writer.writerow(row)



def run(input_dir, output_dir, format, layer):
    arcpy.env.workspace = input_dir
    tables = arcpy.ListTables()
    layers = arcpy.ListFeatureClasses()
    if layer:
        sl = SpatialLayers(tables, layers, layer)
        if sl.target_exists():
            sl = SpatialLayers(tables, layers, layer)
            create_csv(output_dir, sl)
        else:
            sys.exit(f"{layer} does not exist in working environment.")
    else:
        for lyr in tables + layers:
            print(lyr)
            sl = SpatialLayers(tables, layers, lyr)
            create_csv(output_dir, sl)
